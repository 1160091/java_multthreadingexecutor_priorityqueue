Assessment Exercise 1: Communication and coordination with shared memory in Java

Concurrency with shared memory

A call centre for costumer support of a telecommunication company has operators that answer two types of costumerís calls related to:

Configurations
Malfunctions.
Costumer calls are sent to the specific queue (configuration or malfunctions) and are answered as soon as an operator is available. Operators have different skills and answer calls of only one of the two types. Sometimes, due to a wrong understanding of the problem by the costumer, calls go to the wrong queue and operators can, after understanding that it is in the wrong queue, reintroduce the call in the other queue. In order to avoid excessive waiting time these calls have priority over others.  Use Java to simulate all this process.

