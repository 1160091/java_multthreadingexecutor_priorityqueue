import java.util.Queue;
import java.util.concurrent.*;

/**
 * Created by Leonardo Andrade 1160091 on 22-Mar-17.
 */
class CallCenterMain extends Thread {

    private static final int numberOfThreads = 3; // min value = 2;
    private static final int queueCapacity = 5;

    public static void main(String[] args) {
        ExecutorService executorAdd = Executors.newFixedThreadPool(numberOfThreads);
        System.out.println("Starting the load of the Queues of call center randomly:");
        Callable<Queue> addDataToPQC = new CallCenterAddDataToQueueConfiguration(queueCapacity);
        Future<Queue> queueConfigurationLoaded = executorAdd.submit(addDataToPQC);
        Callable<Queue> addDataToPQM = new CallCenterAddDataToQueueMalfunction(queueCapacity);
        Future<Queue> queueMalfunctionLoaded = executorAdd.submit(addDataToPQM);
        Queue readyToPollPQC = null;
        Queue readyToPollPQM = null;
        while (true) {
            /* Hold execution until future is done */
            if (queueConfigurationLoaded.isDone() && queueMalfunctionLoaded.isDone()) {
                try {
                    readyToPollPQC = queueConfigurationLoaded.get();
                    readyToPollPQM = queueMalfunctionLoaded.get();
                } catch (ExecutionException | InterruptedException e) {
                    e.getCause();
                }
                System.out.println("Loading Queues is done");
                break;
            }
        }
        executorAdd.shutdown();
        while (!executorAdd.isTerminated()) {
        }

        if (numberOfThreads > 1) {
            Semaphore semaphore = new Semaphore(1, true);
            ExecutorService executorPoll = Executors.newFixedThreadPool(numberOfThreads);
            Runnable pollFromQueue = new CallCenterPoolDataFromQueueConfiguration(readyToPollPQC, readyToPollPQM, semaphore);
            for (int i = 0; i < numberOfThreads; i++) executorPoll.submit(pollFromQueue);
            System.out.println("\nStarting to unload Queues:");
            executorPoll.shutdown();
            while (!executorPoll.isTerminated()) {
            }
            System.out.println("\nFinished to unload all Queues. \nEnd!");
        } else {
            System.out.println("Min values of threads to poll must be > 2. \nEnd!");
        }


    }
}