import java.util.Queue;
import java.util.concurrent.Semaphore;

class CallCenterPoolDataFromQueueConfiguration extends Thread implements Runnable {

    private Queue<Callers> queueConfiguration = null;
    private Queue<Callers> queueMalfunction = null;
    private Semaphore semaphore;

    CallCenterPoolDataFromQueueConfiguration(Queue<Callers> queueConfiguration,
                                             Queue<Callers> queueMalfunction, Semaphore semaphore) {
        this.queueConfiguration = queueConfiguration;
        this.queueMalfunction = queueMalfunction;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {

        Queue<Callers> PQConfiguration = queueConfiguration;
        Queue<Callers> PQMalfunction = queueMalfunction;

        /* operatorsSkill = 0 --> Configurations // operatorsSkill = 1 --> Malfunctions */
        long operatorsSkill = 0;
        /* Each Thread has your skill and listen to your queue and process it in your time*/
        if ((Thread.currentThread().getId() % 2) == operatorsSkill) {
            System.out.println(Thread.currentThread().getName() + " attending Queue Configuration starting with " + PQConfiguration.size() + " customer(s) in queue");
            while (PQConfiguration.size() > 0 || PQMalfunction.size() > 0) {
                sleepCommand(300);
                try {
                    semaphore.acquire();
                    pollDataFromQueueConfiguration(PQConfiguration, PQMalfunction);
                    System.out.print("\n");
                } catch (InterruptedException ie) {
                    ie.getCause();
                }
                semaphore.release();
            }
        } else {
            System.out.println(Thread.currentThread().getName() + " attending Queue Malfunction starting with " + PQMalfunction.size() + " customer(s) in queue");
            while (PQConfiguration.size() > 0 || PQMalfunction.size() > 0) {
                sleepCommand(600);
                try {
                    semaphore.acquire();
                    pollDataFromQueueMalfunction(PQConfiguration, PQMalfunction);
                    System.out.print("\n");
                } catch (InterruptedException ie) {
                    ie.getCause();
                }
                semaphore.release();
            }
        }
    }

    /* Method to poll data from queue */
    private static void pollDataFromQueueConfiguration(Queue<Callers> PQConfiguration, Queue<Callers> PQMalfunction) {
        if (PQConfiguration.size() > 0) {
            /* getCustomerProblemType --> 0 = Configurations 1 = Malfunctions */
            if ((PQConfiguration.peek().getCustomerProblemType() == 0)) {
                Callers customer = PQConfiguration.poll();
                System.out.println(Thread.currentThread().getName() +
                        " Queue Configurations Answered " + customer.getCustomerName() +
                        " with Problems of Configurations" +
                        " and priority " + customer.getCustomerPriority() +
                        "\n\tStill left " + PQConfiguration.size() + " Customers to attend in Configurations Queue");
            } else {
                Callers customerToBeTransferred = PQConfiguration.poll();
                System.out.println(Thread.currentThread().getName() +
                        " Queue Configurations is transferring the " + customerToBeTransferred.getCustomerName() +
                        " to Queue Malfunctions with high priority!" +
                        "\n\tStill left " + PQConfiguration.size() + " Customers to attend in Configurations Queue");
                customerToBeTransferred.setCustomerPriority();
                PQMalfunction.add(customerToBeTransferred);
            }
        } else System.out.println(Thread.currentThread().getName() + " Queue Configurations empty!" +
                " Nothing to do. Waiting for transferred calls");
    }

    /* Method to poll data from queue */
    private static void pollDataFromQueueMalfunction(Queue<Callers> PQConfiguration, Queue<Callers> PQMalfunction) {
        if (PQMalfunction.size() > 0) {
            /* getCustomerProblemType --> 0 = Configurations 1 = Malfunctions */
            if ((PQMalfunction.peek().getCustomerProblemType() == 1)) {
                Callers customer = PQMalfunction.poll();
                System.out.println(Thread.currentThread().getName() +
                        " Queue Malfunctions Answered " + customer.getCustomerName() +
                        " with Problems of Malfunctions" +
                        " and priority " + customer.getCustomerPriority() +
                        "\n\tStill left " + PQMalfunction.size() + " Customers to attend in Malfunctions Queue");
            } else {

                Callers customerToBeTransferred = PQMalfunction.poll();
                System.out.println(Thread.currentThread().getName() +
                        " Queue Malfunctions is transferring the " + customerToBeTransferred.getCustomerName() +
                        " to Queue Configurations with high priority!" +
                        "\n\tStill left " + PQMalfunction.size() + " Customers to attend in Malfunctions Queue");
                customerToBeTransferred.setCustomerPriority();
                PQConfiguration.add(customerToBeTransferred);
            }
        } else System.out.println(Thread.currentThread().getName() + " Queue Malfunctions empty!" +
                " Nothing to do. Waiting for transferred calls");
    }

    private static void sleepCommand(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}



