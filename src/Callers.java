class Callers {

    private String customerName;
    private int customerProblemType;
    private int customerPriority;

    Callers(String customerName, int customerProblemType, int customerPriority) {
        this.customerName = customerName;
        this.customerProblemType = customerProblemType;
        this.customerPriority = customerPriority;
    }

    String getCustomerName() {
        return customerName;
    }

    int getCustomerProblemType() {
        return customerProblemType;
    }

    void setCustomerPriority() {
        this.customerPriority = 1;
    }

    int getCustomerPriority() {
        return customerPriority;
    }


}
