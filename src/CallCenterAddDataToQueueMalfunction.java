import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Callable;

class CallCenterAddDataToQueueMalfunction implements Callable {

    private int queueCapacity;

    CallCenterAddDataToQueueMalfunction(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    @Override
    public Queue call() {
        /* PriorityQueue with Comparator */
        Queue<Callers> PQMalfunction = new PriorityQueue<>(queueCapacity, idComparator);
        for (int iterator = 1; iterator <= queueCapacity; iterator++) {
            addDataToQueuePQM(PQMalfunction, iterator, queueCapacity);
        }

        return PQMalfunction;
    }

    /* Comparator anonymous class implementation */
    private static Comparator<Callers> idComparator = (c1, c2) -> (int) (c1.getCustomerPriority() - c2.getCustomerPriority());

    /* Method to add random data to Queue */
    private static void addDataToQueuePQM(Queue<Callers> PQMalfunction, int iterator, int queueCapacity) {

        String Name = "Customer Name " + (iterator + queueCapacity);
        /* idxProblemsType --> 0 = Configurations and 1 = Malfunctions */
        String[] problemsType = {"Configurations", "Malfunctions"};
        /* randomizing the value of idxProblemsType to simulate right and wrong calls */
        int idxProblemsType = new Random().nextInt(problemsType.length);
        /* priority --> >10 is normal FIFO and 1 to give highest priority */
        int priority = 10 + iterator;
        PQMalfunction.add(new Callers(Name, idxProblemsType, priority));
        System.out.println(Thread.currentThread().getName() + " Enqueued " + Name +
                " with Priority " + priority +
                " and Problems of " + problemsType[idxProblemsType] +
                " was sent to Queue Malfunctions");
    }
}



